#pragma once

#include <vector>
#include <iostream>

#include <Camera.hpp>
#include <glm/ext.hpp>
#include "Maze.hpp"

class MazeCameraMover : public CameraMover
{
public:
    MazeCameraMover(Maze &maze, bool free = false);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

    void move(glm::vec3 pos);

protected:
    //Положение виртуальный камеры задается в сферических координат
    glm::vec3 _pos;
    glm::quat _rot;
    bool _free;

    //Положение курсора мыши на предыдущем кадре
    double PreviousXPosition = 0.0;
    double PreviousYPosition = 0.0;
private:
    // испльзуемый лабиринт
    Maze &maze;
};
